import React from 'react'
import { Link } from "react-router-dom"

import Header from '../../components/Header/Header'
import ContainerList from '../../components/ContainerList/ContainerList'


class ContainerListPage extends React.Component {

  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="app-body">
          <div className="container-fluid">
            <div className="row">
              <div className="col">
                <nav aria-label="breadcrumb">
                  <ol className="breadcrumb">
                    <li className="breadcrumb-item">Docker</li>
                    <li className="breadcrumb-item active" aria-current="page">Listagem</li>
                  </ol>
                </nav>
              </div>
              <div className="col flex-grow-0">
                <Link to="/docker/container/create" className="btn btn-primary btn-lg">Criar</Link>
              </div>
            </div>
            <ContainerList />
          </div>
        </div>
      </React.Fragment>
    )
  }

}

export default ContainerListPage
