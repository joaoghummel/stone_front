import React from 'react'

import Header from '../../components/Header/Header'


class ContainerCreatePage extends React.Component {

  state = {
    image: '',
    command: '',
    name: '',
    detach: '',
    tty: '',
    isLoading: false
  }

  evalRadioInput = (value) => {
    return value === 'true' ? true : false
  }

  inputChange = (event) => {
    const { name, value } = event.target
    this.setState({
      [name]: value
    });
  }

  submit = async () => {
    let currentState = { ...this.state }
    let body = {
      image: currentState.image,
      command: currentState.command,
      name: currentState.name,
      detach: this.evalRadioInput(currentState.detach),
      tty: this.evalRadioInput(currentState.tty),
    }
    await fetch('http://localhost:5000/docker/container/run', {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(body)
    })
    .then(response => {
      if(!response.ok) { throw response }
      return response.json()
    })
    .then(response => {
      alert(response.message)
      this.props.history.push('/docker/container/list');
    })
    .catch(response=> {
      response.json()
      .then(response=>alert(response.message))
    })
  }

  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="app-body">
          <div className="container">
            <div className="card shadow-sm h-100">
            <h2 className="h4 card-header">Criar container</h2>
            <div className="card-body">
              <form>
                <div className="form-group">
                  <label htmlFor="name">Nome</label>
                  <input id="name"
                         name="name"
                         placeholder="Aplicação XYZ"
                         className="form-control"
                         onChange={this.inputChange} />
                  <small id="nameHelp" className="form-text text-muted">Nome do container</small>
                </div>

                <div className="form-group">
                  <label htmlFor="image">Imagem</label>
                  <input id="image"
                         name="image"
                         placeholder="debian:latest"
                         className="form-control"
                         onChange={this.inputChange} />
                  <small id="imageHelp" className="form-text text-muted">Imager de base para o container</small>
                </div>

                <div className="form-group">
                  <label htmlFor="command">Comando</label>
                  <input id="command"
                         name="command"
                         placeholder="/bin/bash"
                         className="form-control"
                         onChange={this.inputChange} />
                  <small id="commandHelp" className="form-text text-muted">Comando que será utilizado no container</small>
                </div>
                
                <div className="form-group">
                  <label>Destacar</label>
                  <div className="form-check">
                    <input type="radio"
                           id="detach_true"
                           name="detach"
                           value="true"
                           className="form-check-input"
                           onChange={this.inputChange} />
                    <label className="form-check-label" htmlFor="detach_true"><small>Sim</small></label>
                  </div>
  
                  <div className="form-check">
                  <input type="radio"
                         id="detach_false"
                         name="detach"
                         value="true"
                         className="form-check-input"
                         onChange={this.inputChange} />
                    <label className="form-check-label" htmlFor="detach_false"><small>Não</small></label>
                  </div>
                  <small id="commandHelp" className="form-text text-muted">Se o container deve ser destacado.</small>
                </div>
                
                <div className="form-group">
                  <label>TTY</label>
                  <div className="form-check">
                  <input type="radio"
                         id="tty_true"
                         name="tty"
                         value="true"
                         className="form-check-input"
                         onChange={this.inputChange} />
                    <label className="form-check-label" htmlFor="tty_true"><small>Sim</small></label>
                  </div>
  
                  <div className="form-check">
                  <input type="radio"
                         id="tty_false"
                         name="tty"
                         value="true"
                         className="form-check-input"
                         onChange={this.inputChange} />
                    <label className="form-check-label" htmlFor="tty_false"><small>Não</small></label>
                  </div>
                  <small id="commandHelp" className="form-text text-muted">Se deve ser utilizado um pseudo terminal</small>
                </div>
              </form>
            </div>
            <div className="card-footer d-flex">
              <button className="btn btn-success ml-auto" onClick={this.submit}>Criar</button>
            </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }

}

export default ContainerCreatePage
