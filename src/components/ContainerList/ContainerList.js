import React from 'react'

import ContainerCard from './ContainerCard/ContainerCard'
import { tsThisType } from '@babel/types';

class ContainerList extends React.PureComponent {

  state = {
    containerList: []
  }

  componentDidMount = async () => {
    await this.loadContainers()
  }

  loadContainers = async () => {
    await fetch('http://localhost:5000/docker/container/list')
    .then(response => {
      if(!response.ok) { throw response }
      return response.json()
    })
    .then(body => {
      this.setState({containerList: body})
    })
    .catch(response=> {
      response.json()
      .then(response=>alert(response.message))
    })
  }

  uiContainerList = () => {
    return this.state.containerList.map(container => {
      return <ContainerCard
                container={ container }
                loadContainers={ this.loadContainers }
                key={ container.id } />
    })
  }

  render() {
      return <section className="row">
        { this.uiContainerList() }
      </section>
  }
}

export default ContainerList