import React from 'react'

class ContainerCard extends React.PureComponent {

  start = async () => {
    const { id } = this.props.container
    await fetch(`http://localhost:5000/docker/container/start/${id}`)
    .then(response =>  {
      if(!response.ok) { throw response }
      return response.json()
    })
    .then(response => {
      alert(response.message)
      this.props.loadContainers()
    })
    .catch(response=> {
      response.json()
      .then(response=>alert(response.message))
    })
  }

  stop = async () => {
    const { id } = this.props.container
    await fetch(`http://localhost:5000/docker/container/stop/${id}`)
    .then(response =>  {
      if(!response.ok) { throw response }
      return response.json()
    })
    .then(response => {
      alert(response.message)
      this.props.loadContainers()
    })
    .catch(response=> {
      response.json()
      .then(response=>alert(response.message))
    })
  }

  remove = async () => {
    const { id } = this.props.container
    await fetch(`http://localhost:5000/docker/container/remove/${id}`)
    .then(response =>  {
      if(!response.ok) { throw response }
      return response.json()
    })
    .then(response => {
      alert(response.message)
      this.props.loadContainers()
    })
    .catch(response=> {
      response.json()
      .then(response=>alert(response.message))
    })
  }

  render() {
    const { image, name, status } = this.props.container
    return <article className="col-12 col-sm-6 col-md-4 mb-3">
      <div className="card shadow-sm h-100">
        <h2 className="h4 card-header">{ name }</h2>
        <div className="card-body">
          <ul className="list-group list-group-flush">
            <li className="list-group-item"><b>Imagem:</b> { image }</li>
            <li className="list-group-item"><b>status:</b> { status }</li>
          </ul>
        </div>
        <div className="card-footer d-flex">
          <button className="btn btn-success mr-1" onClick={this.start}>Iniciar</button>
          <button className="btn btn-warning" onClick={this.stop}>Parar</button>
          <button className="btn btn-danger ml-auto" onClick={this.remove}>Excluir</button>
        </div>
      </div>
    </article>
  }
}

export default ContainerCard