import React from 'react'
import { Link, NavLink } from 'react-router-dom'

class Header extends React.PureComponent {

  render() {
    return <header className="app-header navbar navbar-expand-lg navbar-dark bg-dark mb-3">
      <Link to="/docker/container/list" className="navbar-brand">App</Link>
      <ul className="navbar-nav">
        <li className="nav-item active">
          <NavLink to="/docker/container/list" className="nav-link">
            Docker <span className="sr-only">(current)</span>
          </NavLink>
        </li>
      </ul>
    </header>
  }
}

export default Header