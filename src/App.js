import React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"

import './App.css'
import ContainerListPage from './pages/ContainerListPage/ContainerListPage'
import ContainerCreatePage from './pages/ContainerCreatePage/ContainerCreatePage'

function App() {

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/docker/container/list" component={ContainerListPage} />
          <Route path="/docker/container/create" component={ContainerCreatePage} />
          <Route component={ContainerListPage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
